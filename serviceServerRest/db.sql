/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Sergi Martínez
 * Created: 02/05/2017
 */

INSERT INTO question (question)
VALUES ("question1");
INSERT INTO question (question)
VALUES ("question2");
INSERT INTO question (question)
VALUES ("question3");
INSERT INTO question (question)
VALUES ("question4");
INSERT INTO question (question)
VALUES ("question5");

INSERT INTO answer (idQuestion, answer, isCorrect)
VALUES (1, "correctAnswer", 1);
INSERT INTO answer (idQuestion, answer, isCorrect)
VALUES (1, "incorrectAnswer", 0);
INSERT INTO answer (idQuestion, answer, isCorrect)
VALUES (1, "incorrectAnswer", 0);
INSERT INTO answer (idQuestion, answer, isCorrect)
VALUES (1, "incorrectAnswer", 0);

INSERT INTO answer (idQuestion, answer, isCorrect)
VALUES (2, "incorrectAnswer", 0);
INSERT INTO answer (idQuestion, answer, isCorrect)
VALUES (2, "correctAnswer", 1);
INSERT INTO answer (idQuestion, answer, isCorrect)
VALUES (2, "incorrectAnswer", 0);
INSERT INTO answer (idQuestion, answer, isCorrect)
VALUES (2, "incorrectAnswer", 0);

INSERT INTO answer (idQuestion, answer, isCorrect)
VALUES (3, "incorrectAnswer", 0);
INSERT INTO answer (idQuestion, answer, isCorrect)
VALUES (3, "correctAnswer", 0);
INSERT INTO answer (idQuestion, answer, isCorrect)
VALUES (3, "incorrectAnswer", 1);
INSERT INTO answer (idQuestion, answer, isCorrect)
VALUES (3, "correctAnswer", 0);

INSERT INTO answer (idQuestion, answer, isCorrect)
VALUES (4, "incorrectAnswer", 0);
INSERT INTO answer (idQuestion, answer, isCorrect)
VALUES (4, "incorrectAnswer", 0);
INSERT INTO answer (idQuestion, answer, isCorrect)
VALUES (4, "correctAnswer", 1);
INSERT INTO answer (idQuestion, answer, isCorrect)
VALUES (4, "incorrectAnswer", 0);


INSERT INTO answer (idQuestion, answer, isCorrect)
VALUES (5, "incorrectAnswer", 0);
INSERT INTO answer (idQuestion, answer, isCorrect)
VALUES (5, "correctAnswer", 1);
INSERT INTO answer (idQuestion, answer, isCorrect)
VALUES (5, "incorrectAnswer", 0);
INSERT INTO answer (idQuestion, answer, isCorrect)
VALUES (5, "incorrectAnswer", 0);