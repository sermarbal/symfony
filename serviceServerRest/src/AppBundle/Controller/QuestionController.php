<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use AppBundle\Entity\Question;
use AppBundle\Entity\Answer;

class QuestionController extends FOSRestController {

    /**
     * @Rest\Get("/question")
     */
    public function getAction() {
        $restresult = $this->getDoctrine()->getRepository('AppBundle:Question')->findAll();
        if ($restresult === null) {
            return new View("there are no users exist", Response::HTTP_NOT_FOUND);
        }
        return $restresult;
    }
    
    /**
     * @Rest\Get("/question/{id}")
     */
    public function idAction($id) {
        $singleresult = $this->getDoctrine()->getRepository('AppBundle:Question')->find($id);
        if ($singleresult === null) {
            return new View("user not found", Response::HTTP_NOT_FOUND);
        }
        return $singleresult;
    }

    /**
     * @Rest\Post("/question")
     */
    public function postAction(Request $request) {

        $data = new Question;
        $question = $request->get('question');
        if (empty($question)) {
            return new View("NULL VALUES ARE NOT ALLOWED", Response::HTTP_NOT_ACCEPTABLE);
        }
        $data->setQuestion($question);
        $em = $this->getDoctrine()->getManager();
        $em->persist($data);
        $em->flush();

        return new View("Question Added Successfully", Response::HTTP_OK);
    }

    /**
     * @Rest\Put("/question/{id}")
     */
    public function updateAction($id, Request $request) {

        $question = $request->get('question');
        $em = $this->getDoctrine()->getManager();
        $questionData = $this->getDoctrine()->getRepository('AppBundle:Question')->find($id);
        if (empty($questionData)) {
            return new View("Question not found", Response::HTTP_NOT_FOUND);
        } elseif (!empty($question)) {
            $questionData->setQuestion($question);
            $em->flush();
            return new View("Question Updated Successfully", Response::HTTP_OK);
        } else{
            return new View("Question question cannot be empty", Response::HTTP_NOT_ACCEPTABLE);
        }
    }

    /**
     * @Rest\Delete("/question/{id}")
     */
    public function deleteAction($id) {
        $em = $this->getDoctrine()->getManager();
        $question = $this->getDoctrine()->getRepository('AppBundle:Question')->find($id);
        $answers = $this->getDoctrine()->getRepository('AppBundle:Answer')->findOneBy(array('idQuestion' => $id));
        if (!empty($answers)) {
        return new View("Question has foreign key associated", Response::HTTP_NOT_ACCEPTABLE);    
        } else if (empty($question)){
            return new View("Question not found", Response::HTTP_NOT_FOUND);            
        } else {
            $em->remove($question);
            $em->flush();
        }
        return new View("Deleted successfully", Response::HTTP_OK);
    }
}
