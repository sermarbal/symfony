<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use AppBundle\Entity\Question;
use AppBundle\Entity\Answer;

class AnswerController extends FOSRestController {

    /**
     * @Rest\Get("/answer")
     */
    public function getAction() {
        $restresult = $this->getDoctrine()->getRepository('AppBundle:Answer')->findAll();
        if ($restresult === null) {
            return new View("There are no answers exists", Response::HTTP_NOT_FOUND);
        }
        return $restresult;
    }

    /**
     * @Rest\Get("/answer/{id}")
     */
    public function idAction($id) {
        $singleresult = $this->getDoctrine()->getRepository('AppBundle:Answer')->find($id);
        if ($singleresult === null) {
            return new View("Answer not found", Response::HTTP_NOT_FOUND);
        }
        return $singleresult;
    }

    /**
     * @Rest\Post("/answer")
     */
    public function postAction(Request $request) {

        $data = new Answer;
        $answer = $request->get('answer');
        $isCorrect = $request->get('isCorrect');
        $idQuestion = $request->get('idQuestion');
        
        $question = $this->getDoctrine()->getRepository('AppBundle:Question')->find($idQuestion);
        
        if (empty($answer) || empty($isCorrect) || empty($idQuestion)) {
            return new View("NULL VALUES ARE NOT ALLOWED", Response::HTTP_NOT_ACCEPTABLE);
        } else if(empty($question)){
            return new View("Question not exists", Response::HTTP_NOT_ACCEPTABLE);
        }
        
        $data->setAnswer($answer);
        $data->setIsCorrect($isCorrect);
        $data->setIdQuestion($question);
        
        $em = $this->getDoctrine()->getManager();
        $em->persist($data);
        $em->flush();

        return new View("Answer Added Successfully", Response::HTTP_OK);
    }

    /**
     * @Rest\Put("/answer/{id}")
     */
    public function updateAction($id, Request $request) {
        
        $answer = $request->get('answer');
        $isCorrect = $request->get('isCorrect');
        $idQuestion = $request->get('idQuestion');
        
        $em = $this->getDoctrine()->getManager();
        $AnswerData = $this->getDoctrine()->getRepository('AppBundle:Answer')->find($id);
        
        if (empty($AnswerData)) {
            return new View("Answer not found", Response::HTTP_NOT_FOUND);
        } elseif (!empty($answer) && (!empty($isCorrect) || $isCorrect==0) && !empty($idQuestion)) {
            $AnswerData->setAnswer($answer);
            $AnswerData->setIsCorrect($isCorrect);
            $question = $this->getDoctrine()->getRepository('AppBundle:Question')->find($idQuestion);
            $AnswerData->setIdQuestion($question);
            $em->flush();
            return new View("1 Answer Updated Successfully", Response::HTTP_OK);
        } else if(!empty($answer) && (!empty($isCorrect) || $isCorrect==0)){
            $AnswerData->setAnswer($answer);
            $AnswerData->setIsCorrect($isCorrect);
            $em->flush();
            return new View("2 Answer Updated Successfully", Response::HTTP_OK);
        } else if(!empty($answer) && !empty($idQuestion)){
            $AnswerData->setAnswer($answer);
            $question = $this->getDoctrine()->getRepository('AppBundle:Question')->find($idQuestion);
            $AnswerData->setIdQuestion($question);
            $em->flush();
            return new View("3 Answer Updated Successfully", Response::HTTP_OK);
        } else if((!empty($isCorrect) || $isCorrect==0) && !empty($idQuestion)){
            $AnswerData->setIsCorrect($isCorrect);
            $question = $this->getDoctrine()->getRepository('AppBundle:Question')->find($idQuestion);
            $AnswerData->setIdQuestion($question);
            $em->flush();
            return new View("4 Answer Updated Successfully", Response::HTTP_OK);
        } else if(!empty($answer)){
            $AnswerData->setAnswer($answer);
            $em->flush();
            return new View("5 Answer Updated Successfully", Response::HTTP_OK);
        } else if((!empty($isCorrect) || $isCorrect==0)){
            $AnswerData->setIsCorrect($isCorrect);
            $em->flush();
            return new View("6 Answer Updated Successfully", Response::HTTP_OK);
        } else if(!empty($idQuestion)){
            $question = $this->getDoctrine()->getRepository('AppBundle:Question')->find($idQuestion);
            $AnswerData->setIdQuestion($question);
            $em->flush();
            return new View("7 Answer Updated Successfully", Response::HTTP_OK);
        }
        else{
            return new View("Question question cannot be empty", Response::HTTP_NOT_ACCEPTABLE);
        }
        
    }

    /**
     * @Rest\Delete("/answer/{id}")
     */
    public function deleteAction($id) {
        
        $em = $this->getDoctrine()->getManager();
        $answer = $this->getDoctrine()->getRepository('AppBundle:Answer')->find($id);
        if (empty($answer)){
            return new View("Answer not found", Response::HTTP_NOT_FOUND);            
        } else {
            $em->remove($answer);
            $em->flush();
        }
        return new View("Deleted successfully", Response::HTTP_OK);
        
    }
    
}