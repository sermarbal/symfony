$(document).ready(function () {

    // A menys que el servidor ho digui no es pot tirar els daus.
    var dices = true;
    // Caselles a moure la fitxa del jugador.
    var move = null;
    // Arrays caselles
    var cercle = [];
    var l1 = [];
    var l2 = [];
    var l3 = [];
    var l4 = [];
    var center = [];
    var boxes = [cercle, l1, l2, l3, l4, center];
    // ArraysBoxesMovement
    // in PHP...
    var boxestoSee = [];
    var boxesViewed = [];


    // Array creation

    // Array 1 = external circle
    $(".a1").each(function () {

        switch ($(this).attr("id")) {
            case "c0":
                cercle.push([$(this), [$("#l1c0")]]);
                break;
            case "c10":
                cercle.push([$(this), [$("#l2c0")]]);
                break;
            case "c20":
                cercle.push([$(this), [$("#l3c0")]]);
                break;
            case "c30":
                cercle.push([$(this), [$("#l4c0")]]);
                break;
            default:
                cercle.push([$(this)]);
                break;
        }
    });

    // Array 2 = Line 1
    $(".a2").each(function () {

        switch ($(this).attr("id")) {
            case "l1c0":
                l1.push([$(this), [$("#c0")]]);
                break;
            case "l1c2":
                l1.push([$(this), [$("#center")]]);
                break;
            default:
                l1.push([$(this)]);
                break;
        }
    });

    // Array 3 = Line 2
    $(".a3").each(function () {
        switch ($(this).attr("id")) {
            case "l2c0":
                l2.push([$(this), [$("#c10")]]);
                break;
            case "l2c2":
                l2.push([$(this), [$("#center")]]);
                break;
            default:
                l2.push([$(this)]);
                break;
        }
    });

    // Array 4 = Line 3
    $(".a4").each(function () {
        switch ($(this).attr("id")) {
            case "l3c0":
                l3.push([$(this), [$("#c20")]]);
                break;
            case "l3c2":
                l3.push([$(this), [$("#center")]]);
                break;
            default:
                l3.push($(this));
                break;
        }
    });

    // Array 5 = Line 4
    $(".a5").each(function () {
        switch ($(this).attr("id")) {
            case "l4c0":
                l4.push([$(this), [$("#c30")]]);
                break;
            case "l4c2":
                l4.push([$(this), [$("#center")]]);
                break;
            default:
                l4.push($(this));
                break;
        }
    });

    // Array 6 = Center
    center.push([$("#center"), [$("#l1c2"), $("#l2c2"), $("#l3c2"), $("#l4c2")]]);

    showArrayBoxes(boxes);

    /*
     * Mostra per consola tot l'array general del joc.
     * 
     * @param {Array} 
     * @returns {undefined}
     */
    function showArrayBoxes(array) {
        for (var i = 0; i < array.length; i++) {
            console.log("Array " + i + ":");
            for (var j = 0; j < array[i].length; j++)
                if (array[i][j] instanceof Array) {
                    console.log(j + ": " + array[i][j][0].attr("id") + " Casella amb relacions");
                    for (var k = 0; k < array[i][j].length; k++) {
                        if (array[i][j][k] instanceof Array) {
                            console.log("\t\tArray object relacionats relacions");
                            for (var l = 0; l < array[i][j][k].length; l++)
                                console.log("\t\t" + l + ": " + array[i][j][k][l].attr("id"));
                        }
                    }
                } else
                    console.log(j + ": " + array[i][j].attr("id"));
        }

    }

    /*
     * Listener pel click dels daus
     */
    $("#diceBtn").click(function () {
        if (dices === true) {

            // Comprovació a la bbdd de si ja ha tirat. "tirada" (falta afegir)

            // Petición a PHP para recibir tirada de dados aleatoria entre 1 i 6.
            $.get("./../ajax/test.php", {"casa": "casa"}, function (data) {
                move = data;
                $("#dice").val("La teva tirada: " + move);
            });
//            move = 2;

            dices = false;

            //Sabent la posició actual del jugador actual...

            var position = getPositionById("c0");

            processBoxes(position);

        } else
        if (move !== null) {
            $("#dice").val("Ja has tirat: " + move);
        } else
            $("#dice").val("No et toca tirar!");
    });

    /*
     * A partir d'una id obté la posició de l'array referent a aquesta.
     * @param {string} id id a procesar.
     * @returns {Array} Array amb la posició
     */
    function getPositionById(id) {

        var position = [];

        if (id.startsWith("center")) {
            position[0] = 5;
            position[1] = 0;
        } else if (id.startsWith("c")) {
            position[0] = 0;
            position[1] = id.substring(1);
        } else if (id.startsWith("l1c")) {
            position[0] = 1;
            position[1] = id.substring(3);
        } else if (id.startsWith("l2c")) {
            position[0] = 2;
            position[1] = id.substring(3);
        } else if (id.startsWith("l3c")) {
            position[0] = 3;
            position[1] = id.substring(3);
        } else if (id.startsWith("l4c")) {
            position[0] = 4;
            position[1] = id.substring(3);
        }
        if (position[0] !== 5)
            position[1] = parseInt(position[1]);

        return position;

    }

    function getBoxByArrayPosition(position) {

        return boxes[position[0]][position[1]][0];

    }

    /*
     * Executa la obtencio de les caselles clicables
     * 
     * @param {Integer} array a utilitzar (dintre de boxes)
     * @param {Integer} position
     * @returns {undefined}
     */
    function processBoxes(position) {

        boxestoSee = [boxes[position[0]][position[1]][0]];
        boxesViewed = [boxes[position[0]][position[1]][0]];

        // Vegades que es repetirà el proces per a veure les caselles.

        for (var i = 0; i < move; i++) {


        var aux = boxestoSee;

            // Obtenim nous boxes.
            for (var j = 0; j < aux.length; j++) {
                // Introduir boxes vistos a boxesViewed.
                updateBoxesViewed();
                position = getPositionById(aux[j].attr("id"));
                getNearBoxes(position);
                seeObjectsByArray("before boxestoSee: ", boxestoSee);
                updateBoxestoSee();
                seeObjectsByArray("after boxestoSee: ", boxestoSee);
                seeObjectsByArray("boxesViewed: ", boxesViewed);

            }






        }
        makeBoxesClickable(boxestoSee);

    }

    /*
     * 
     * Funció que reuneix en un array les caselles properes a la casella de l'array i posició donat.
     * 
     * @param {Integer} array
     * @param {Integer} posició a l'array
     * @returns {Array} Array amb objectes casella propers.
     * 
     */
    function getNearBoxes(position) {

        //alert(position[0]+" "+position[1])

        // Mirem els seus germans d'array
        // 
        // Està a l'inici del array?
        if (position[1] === 0 && position[0] !== 5) {
            boxestoSee.push(boxes[position[0]][boxes[position[0]].length - 1][0]);
            boxestoSee.push(boxes[position[0]][position[1] + 1][0]);
        }
        // Està al final de l'array?
        else if (position[1] === boxes[position[0]].length - 1 && position[0] !== 5) {
            boxestoSee.push(boxes[position[0]][position[1] - 1][0]);
            boxestoSee.push(boxes[position[0]][0][0]);
        } else if (position[0] !== 5) {
            boxestoSee.push(boxes[position[0]][position[1] - 1][0]);
            boxestoSee.push(boxes[position[0]][position[1] + 1][0]);
        }

        // Mirem is te relacions.
        if (boxes[position[0]][position[1]].length > 1)
            for (var i = 0; i < boxes[position[0]][position[1]][1].length; i++) {
                boxestoSee.push(boxes[position[0]][position[1]][1][i]);
            }

        return boxestoSee;

    }

    /*
     * Funcio per a testejar.
     */
    function test() {
//        boxesViewed = [$("#l1c0")];
//        boxestoSee = [$("#c0"), $("#c1"), $("#l1c0"), $("#c39")];

//        seeObjectsByArray("before boxestoSee: ", boxestoSee);
//        updateBoxestoSee(boxesViewed, boxestoSee);
//        seeObjectsByArray("after boxestoSee: ", boxestoSee);

//        alert("before: " + boxesViewed);
//        updateBoxesViewed(boxesViewed, boxestoSee);
//        alert("after: " + boxesViewed);

        var p1 = "1";
        putPlayer(p1, "center");

//        makeBoxesClickable(boxestoSee);

//        seeObjectsByArray("before boxestoSee: ", boxestoSee);
//        seeObjectsByArray("before boxesViewed: ", boxesViewed);
//
//        updateBoxestoSee();
//        updateBoxesViewed();
//
//        seeObjectsByArray("after boxestoSee: ", boxestoSee);
//        seeObjectsByArray("after boxesViewed: ", boxesViewed);

//        boxestoSee = getNearBoxes([1,0]);
//        makeBoxesClickable(boxestoSee);
    }

    function seeObjectsByArray(s, array) {
        for (var i = 0; i < array.length; i++)
            s += " " + i + "[" + array[i].attr("id") + "] ";
        alert(s);
    }

    /*
     * Mira l'array de caselles vistes i elimina de l'array de caselles per veure els que hi hagi a caselles vistes.
     * 
     * @param {Array} boxesViewed
     * @param {Array} boxestoSee
     * @returns {undefined}
     */
    function updateBoxestoSee() {
        
        var aux = boxestoSee;
        // Esborrem les referencies a objectes de boxestoSee que existeixin a boxesViewed
        for (var i = 0; i < boxesViewed.length; i++) {
            for (var j = 0; j < boxestoSee.length; j++) {
                if (boxesViewed[i].attr("id") === boxestoSee[j].attr("id")) {
                    aux[j]=null;
                    break;
                }
            }
        }
        boxestoSee = [];
        for (var i = 0; i < aux.length; i++) {
            if(aux[i]!=null) 
               boxestoSee.push(aux[i]);
        }
        
    }

    /*
     * Mira les caselles que s'han de veure i s'hafegeixen a l'array de caselles vistes.
     */
    function updateBoxesViewed() {

        // Afegim els objectes que no existeixin a boxesViewed pero si a boxestosee
        for (var i = 0; i < boxestoSee.length; i++) {
            var exists = false;
            for (var j = 0; j < boxesViewed.length; j++) {
                if (boxestoSee[i].attr("id") === boxesViewed[j].attr("id")) {
                    exists = true;
                    break;
                }
            }
            if (!exists)
                boxesViewed.push(boxestoSee[i]);

        }

    }

    function putPlayer(player, box) {
        if ($("#p" + player).attr("id") !== null)
            $("#p" + player).remove();
        else {
        }
        box = $("#" + box);
        var playerBox = null;
        // Mirem tipus casella desti.
        if (box.attr("class").includes("t1"))
            playerBox = $("<div id='p" + player + "' class='fitxa player" + player + "'></div>");
        else if (box.attr("class").includes("t2")) {
            playerBox = $("<div id='p" + player + "' class='fitxaP player" + player + "'></div>");
        } else
            playerBox = $("<div id='p" + player + "' class='fitxaG player" + player + "'></div>");
        box.append(playerBox);
    }

    function makeBoxesClickable(boxes) {
        for (var i = 0; i < boxes.length; i++) {
            boxes[i].on('click', clickable);
            boxes[i].addClass("moveOption");
        }
    }

    function undoBoxesClickable(boxes) {
        for (var i = 0; i < boxes.length; i++) {
            boxes[i].off('click', clickable);
            boxes[i].removeClass("moveOption");
        }
    }

    function clickable() {
        //alert($(this).attr("id")+": "+$(this).attr("class"));
        makeQuestion($(this).attr("class"));
        undoBoxesClickable(boxestoSee);
    }

    function makeQuestion(classes) {
        alert(classes);
        // Jquery ui con question del tipus corresponent
        if (classes.includes("programming")) {
            alert("q programming");
        } else if (classes.includes("movies"))
            alert("q movies");
        else if (classes.includes("psychology"))
            alert("q psychology");
        else if (classes.includes("sports"))
            alert("q sports");
        else
            alert("q final or combat");

    }

    test();

});