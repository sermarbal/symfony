<?php

// src/AppBundle/Entity/User.php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * @ORM\Entity
 * @ORM\Table(name="Users")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 */
class User extends BaseUser {
    // Els altres camps ja estan introduits via FOSUserBundle
    // email
    // Username
    // password (encrypted)

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotBlank(message="Please enter your name.", groups={"Registration", "Profile"})
     * @Assert\Length(
     *     min=3,
     *     max=255,
     *     minMessage="The name is too short.",
     *     maxMessage="The name is too long.",
     *     groups={"Registration", "Profile"}
     * )
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotBlank(message="Please enter your lastname.", groups={"Registration", "Profile"})
     * @Assert\Length(
     *     min=3,
     *     max=255,
     *     minMessage="The name is too short.",
     *     maxMessage="The name is too long.",
     *     groups={"Registration", "Profile"}
     * )
     */
    protected $lastname;

    /**
     * @ORM\OneToMany(targetEntity="Cheese", mappedBy="idUser")
     */
    protected $cheeses;

    public function __construct()
    {
        parent::__construct();
        $this->cheeses = new ArrayCollection();
    }
    
    /**
     * Set name
     *
     * @param string $name
     *
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     *
     * @return User
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Add cheese
     *
     * @param \AppBundle\Entity\Cheese $cheese
     *
     * @return User
     */
    public function addCheese(\AppBundle\Entity\Cheese $cheese)
    {
        $this->cheeses[] = $cheese;

        return $this;
    }

    /**
     * Remove cheese
     *
     * @param \AppBundle\Entity\Cheese $cheese
     */
    public function removeCheese(\AppBundle\Entity\Cheese $cheese)
    {
        $this->cheeses->removeElement($cheese);
    }

    /**
     * Get cheeses
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCheeses()
    {
        return $this->cheeses;
    }
}
