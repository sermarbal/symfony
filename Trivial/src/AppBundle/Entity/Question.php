<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="Questions")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\QuestionRepository")
 */
class Question {
    
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\Column(type="string", length=300)
     */
    protected $question;
    
    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $answer1;
    
    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $answer2;
    
    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $answer3;
            
    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $answer4;
    
    /**
     * @ORM\Column(type="integer")
     */
    protected $correct_Answer;
    
    /**
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="Questions")
     * @ORM\JoinColumn(name="id_Category", referencedColumnName="id")
     */
    protected $id_Category;
    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set question
     *
     * @param string $question
     *
     * @return Question
     */
    public function setQuestion($question)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return string
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set answer1
     *
     * @param string $answer1
     *
     * @return Question
     */
    public function setAnswer1($answer1)
    {
        $this->answer1 = $answer1;

        return $this;
    }

    /**
     * Get answer1
     *
     * @return string
     */
    public function getAnswer1()
    {
        return $this->answer1;
    }

    /**
     * Set answer2
     *
     * @param string $answer2
     *
     * @return Question
     */
    public function setAnswer2($answer2)
    {
        $this->answer2 = $answer2;

        return $this;
    }

    /**
     * Get answer2
     *
     * @return string
     */
    public function getAnswer2()
    {
        return $this->answer2;
    }

    /**
     * Set answer3
     *
     * @param string $answer3
     *
     * @return Question
     */
    public function setAnswer3($answer3)
    {
        $this->answer3 = $answer3;

        return $this;
    }

    /**
     * Get answer3
     *
     * @return string
     */
    public function getAnswer3()
    {
        return $this->answer3;
    }

    /**
     * Set answer4
     *
     * @param string $answer4
     *
     * @return Question
     */
    public function setAnswer4($answer4)
    {
        $this->answer4 = $answer4;

        return $this;
    }

    /**
     * Get answer4
     *
     * @return string
     */
    public function getAnswer4()
    {
        return $this->answer4;
    }

    /**
     * Set correct_Answer
     *
     * @param integer $correct_Answer
     *
     * @return Question
     */
    public function setcorrect_Answer($correct_Answer)
    {
        $this->correct_Answer = $correct_Answer;

        return $this;
    }

    /**
     * Get correct_Answer
     *
     * @return integer
     */
    public function getcorrect_Answer()
    {
        return $this->correct_Answer;
    }

    /**
     * Set id_Category
     *
     * @param \AppBundle\Entity\Category $id_Category
     *
     * @return Question
     */
    public function setid_Category(\AppBundle\Entity\Category $id_Category = null)
    {
        $this->id_Category = $id_Category;

        return $this;
    }

    /**
     * Get id_Category
     *
     * @return \AppBundle\Entity\Category
     */
    public function getid_Category()
    {
        return $this->id_Category;
    }

    /**
     * Set correctAnswer
     *
     * @param integer $correctAnswer
     *
     * @return Question
     */
    public function setCorrectAnswer($correctAnswer)
    {
        $this->correct_Answer = $correctAnswer;

        return $this;
    }

    /**
     * Get correctAnswer
     *
     * @return integer
     */
    public function getCorrectAnswer()
    {
        return $this->correct_Answer;
    }

    /**
     * Set idCategory
     *
     * @param \AppBundle\Entity\Category $idCategory
     *
     * @return Question
     */
    public function setIdCategory(\AppBundle\Entity\Category $idCategory = null)
    {
        $this->id_Category = $idCategory;

        return $this;
    }

    /**
     * Get idCategory
     *
     * @return \AppBundle\Entity\Category
     */
    public function getIdCategory()
    {
        return $this->id_Category;
    }
}
