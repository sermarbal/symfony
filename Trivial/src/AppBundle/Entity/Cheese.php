<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="Cheeses")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CheeseRepository")
 */
class Cheese {

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="User", inversedBy="cheeses")
     * @ORM\JoinColumn(name="idUser", referencedColumnName="id")
     */
    protected $idUser;
    
    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Game", inversedBy="cheeses")
     * @ORM\JoinColumn(name="idGame", referencedColumnName="id")
     */
    protected $idGame;
    
    /**
     * @ORM\Column(type="integer")
     */
    protected $posPlayer;
    
    /**
     * @ORM\Column(type="string")
     */
    protected $posBox;
    
    /**
     * @ORM\Column(type="boolean")
     */
    protected $cheeseSports;
    
    /**
     * @ORM\Column(type="boolean")
     */
    protected $cheesePsychology;
    
    /**
     * @ORM\Column(type="boolean")
     */
    protected $cheeseProgramming;
    
    /**
     * @ORM\Column(type="boolean")
     */
    protected $cheeseMovies;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add idUser
     *
     * @param \AppBundle\Entity\User $idUser
     *
     * @return Cheese
     */
    public function addIdUser(\AppBundle\Entity\User $idUser)
    {
        $this->idUser[] = $idUser;

        return $this;
    }

    /**
     * Remove idUser
     *
     * @param \AppBundle\Entity\User $idUser
     */
    public function removeIdUser(\AppBundle\Entity\User $idUser)
    {
        $this->idUser->removeElement($idUser);
    }

    /**
     * Get idUser
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * Set pos
     *
     * @param integer $pos
     *
     * @return Cheese
     */
    public function setPos($pos)
    {
        $this->pos = $pos;

        return $this;
    }

    /**
     * Get pos
     *
     * @return integer
     */
    public function getPos()
    {
        return $this->pos;
    }

    /**
     * Set cheeseSports
     *
     * @param boolean $cheeseSports
     *
     * @return Cheese
     */
    public function setCheeseSports($cheeseSports)
    {
        $this->cheeseSports = $cheeseSports;

        return $this;
    }

    /**
     * Get cheeseSports
     *
     * @return boolean
     */
    public function getCheeseSports()
    {
        return $this->cheeseSports;
    }

    /**
     * Set cheesePsychology
     *
     * @param boolean $cheesePsychology
     *
     * @return Cheese
     */
    public function setCheesePsychology($cheesePsychology)
    {
        $this->cheesePsychology = $cheesePsychology;

        return $this;
    }

    /**
     * Get cheesePsychology
     *
     * @return boolean
     */
    public function getCheesePsychology()
    {
        return $this->cheesePsychology;
    }

    /**
     * Set cheeseProgramming
     *
     * @param boolean $cheeseProgramming
     *
     * @return Cheese
     */
    public function setCheeseProgramming($cheeseProgramming)
    {
        $this->cheeseProgramming = $cheeseProgramming;

        return $this;
    }

    /**
     * Get cheeseProgramming
     *
     * @return boolean
     */
    public function getCheeseProgramming()
    {
        return $this->cheeseProgramming;
    }

    /**
     * Set cheeseMovies
     *
     * @param boolean $cheeseMovies
     *
     * @return Cheese
     */
    public function setCheeseMovies($cheeseMovies)
    {
        $this->cheeseMovies = $cheeseMovies;

        return $this;
    }

    /**
     * Get cheeseMovies
     *
     * @return boolean
     */
    public function getCheeseMovies()
    {
        return $this->cheeseMovies;
    }

    /**
     * Add idGame
     *
     * @param \AppBundle\Entity\Game $idGame
     *
     * @return Cheese
     */
    public function addIdGame(\AppBundle\Entity\Game $idGame)
    {
        $this->idGame[] = $idGame;

        return $this;
    }

    /**
     * Remove idGame
     *
     * @param \AppBundle\Entity\Game $idGame
     */
    public function removeIdGame(\AppBundle\Entity\Game $idGame)
    {
        $this->idGame->removeElement($idGame);
    }

    /**
     * Get idGame
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIdGame()
    {
        return $this->idGame;
    }

    /**
     * Set idUser
     *
     * @param \AppBundle\Entity\User $idUser
     *
     * @return Cheese
     */
    public function setIdUser(\AppBundle\Entity\User $idUser)
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * Set idGame
     *
     * @param \AppBundle\Entity\Game $idGame
     *
     * @return Cheese
     */
    public function setIdGame(\AppBundle\Entity\Game $idGame)
    {
        $this->idGame = $idGame;

        return $this;
    }

    /**
     * Set posPlayer
     *
     * @param integer $posPlayer
     *
     * @return Cheese
     */
    public function setPosPlayer($posPlayer)
    {
        $this->posPlayer = $posPlayer;

        return $this;
    }

    /**
     * Get posPlayer
     *
     * @return integer
     */
    public function getPosPlayer()
    {
        return $this->posPlayer;
    }

    /**
     * Set posBox
     *
     * @param string $posBox
     *
     * @return Cheese
     */
    public function setPosBox($posBox)
    {
        $this->posBox = $posBox;

        return $this;
    }

    /**
     * Get posBox
     *
     * @return string
     */
    public function getPosBox()
    {
        return $this->posBox;
    }
}
