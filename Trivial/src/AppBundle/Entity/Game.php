<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="Games")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\GameRepository")
 */
class Game {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * 
     */
    protected $id;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $active;
    
    /**
     * @ORM\Column(type="integer")
     */
    protected $turn;
    
    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $winner;
    
    /**
     * @ORM\OneToMany(targetEntity="Cheese", mappedBy="idGame")
     */
    protected $cheeses;

    public function __construct()
    {
        parent::__construct();
        $this->cheeses = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Game
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set turn
     *
     * @param integer $turn
     *
     * @return Game
     */
    public function setTurn($turn)
    {
        $this->turn = $turn;

        return $this;
    }

    /**
     * Get turn
     *
     * @return integer
     */
    public function getTurn()
    {
        return $this->turn;
    }

    /**
     * Set winner
     *
     * @param integer $winner
     *
     * @return Game
     */
    public function setWinner($winner)
    {
        $this->winner = $winner;

        return $this;
    }

    /**
     * Get winner
     *
     * @return integer
     */
    public function getWinner()
    {
        return $this->winner;
    }

    /**
     * Add cheese
     *
     * @param \AppBundle\Entity\Cheese $cheese
     *
     * @return Game
     */
    public function addCheese(\AppBundle\Entity\Cheese $cheese)
    {
        $this->cheeses[] = $cheese;

        return $this;
    }

    /**
     * Remove cheese
     *
     * @param \AppBundle\Entity\Cheese $cheese
     */
    public function removeCheese(\AppBundle\Entity\Cheese $cheese)
    {
        $this->cheeses->removeElement($cheese);
    }

    /**
     * Get cheeses
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCheeses()
    {
        return $this->cheeses;
    }
}
