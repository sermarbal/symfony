<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class CheeseRepository extends EntityRepository {

    // Retorna l'usuari si està en una partida
    public function getUserinGame($idUser) {
        return $this->getEntityManager()
                        ->createQuery(
                                "SELECT c FROM AppBundle:Cheese c WHERE c.idUser=" . $idUser
                        )
                        ->getSingleResult();
    }

    public function getGames() {
//        return $this->getEntityManager()
//            ->createQuery(
//                "SELECT c FROM AppBundle:Cheese c "
//            )
//            ->getResult();
        return $this->getEntityManager()
            ->createQuery(
                "SELECT (c.idGame), (count(c.idUser)) FROM AppBundle:Cheese c GROUP BY c.idGame"
            )
            ->getResult();

//        $em = $this->getDoctrine()->getManager();
//        $query = $em->createQueryBuilder()
//                ->from('AppBundle:Cheese', 'c')
//                ->select("(c.idGame), (g.id)")
//                ->leftJoin("Game", "g", "WITH", "c.idGame=g.id")
//                ->getQuery();
//        
//        $result = $query->getResults();
//
//        return $result;
        
        //"SELECT count(c.idUser) FROM AppBundle:Cheese c GROUP BY c.idGame"
        // select idGame, count(*) FROM Cheeses GROUP BY idGame;
    }

}
