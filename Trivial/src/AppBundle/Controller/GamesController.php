<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class GamesController extends Controller {

    /**
     * @Route("/games", name="games")
     */
    public function GamesAction(Request $request) {

        $games = null;
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();

        // Si l'usuari està loggat
        if ($user != null) {

            // Mirem si l'usuari està en una partida...
            $cheese = $em->getRepository('AppBundle:Cheese')
                    ->getUserinGame($user->getId());

            // Si l'usuari està en una partida va cap el game pertinent.
//            if($cheese != null){
//                return $this->redirectToRoute('/game/'.$cheese->getIdGame()->getId());
//            }
            // Si no ho està mostra els games
//            else{

            $games = $em->getRepository('AppBundle:Cheese')
                    ->getGames();

//            $games = $em->getRepository('AppBundle:Cheese')
//                    ->findByIdUser($games->getIdUser());
            if($games !=null){
            return $this->render('gamesList.html.twig', array(
                        'games' => $games));
            }
            return $this->render('test.html.twig', array(
                        'games' => $games));
        }
        // Si no ho està
        else {
            return new Response("You are not logged!");
        }
    }

    /**
     * @Route("/game/{idGame}", name="game")
     */
    public function getGameAction($idGame) {
        // current user id
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();


        // current user from bd
        $userGame = $em->getRepository('AppBundle:Cheese')
                ->getUserinGame($user->getId());

        if ($userGame->getIdUser()->getId() == $idGame){
            return $this->render('game.html.twig');
        }
        return $this->render('test.html.twig', array(
                    'games' => "No estas a aquesta partida."));
    }

    public function getGamePlayers() {
        
    }

}
