/* Game Inserts */

INSERT INTO Games(active, turn, winner) VALUES (true, 1, null);
INSERT INTO Games(active, turn, winner) VALUES (true, 1, null);
INSERT INTO Games(active, turn, winner) VALUES (true, 1, null);

/* Cheese Inserts */

INSERT INTO Cheeses(idUser, idGame, pos_player, pos_box, cheese_sports, cheese_psychology, cheese_programming, cheese_movies) values(1, 1, 1, "c0", false, false, false, false);
INSERT INTO Cheeses(idUser, idGame, pos_player, pos_box, cheese_sports, cheese_psychology, cheese_programming, cheese_movies) values(2, 1, 2, "c0", false, false, false, false);
INSERT INTO Cheeses(idUser, idGame, pos_player, pos_box, cheese_sports, cheese_psychology, cheese_programming, cheese_movies) values(3, 1, 3, "c0", false, false, false, false);
INSERT INTO Cheeses(idUser, idGame, pos_player, pos_box, cheese_sports, cheese_psychology, cheese_programming, cheese_movies) values(4, 1, 4, "c0", false, false, false, false);

/* Category / Questions inserts */

INSERT INTO Categories(name_Category) values("Sports"); /* Red */
INSERT INTO Categories(name_Category) values("Science"); /* Green */
INSERT INTO Categories(name_Category) values("Programming"); /* Blue */
INSERT INTO Categories(name_Category) values("Movies"); /* Light-Blue */

/* Sports */
INSERT INTO Questions(question, answer1, answer2, answer3, answer4, correct_Answer, id_Category)
VALUES ("q1", "a1","a2","a3","a4", 1, 1);
INSERT INTO Questions(question, answer1, answer2, answer3, answer4, correct_Answer, id_Category)
VALUES ("q2", "a1","a2","a3","a4", 1, 1);
INSERT INTO Questions(question, answer1, answer2, answer3, answer4, correct_Answer, id_Category)
VALUES ("q3", "a1","a2","a3","a4", 1, 1);
INSERT INTO Questions(question, answer1, answer2, answer3, answer4, correct_Answer, id_Category)
VALUES ("q4", "a1","a2","a3","a4", 1, 1);
INSERT INTO Questions(question, answer1, answer2, answer3, answer4, correct_Answer, id_Category)
VALUES ("q5", "a1","a2","a3","a4", 1, 1);

/* Science */
INSERT INTO Questions(question, answer1, answer2, answer3, answer4, correct_Answer, id_Category)
VALUES ("q1", "a1","a2","a3","a4", 1, 2);
INSERT INTO Questions(question, answer1, answer2, answer3, answer4, correct_Answer, id_Category)
VALUES ("q2", "a1","a2","a3","a4", 1, 2);
INSERT INTO Questions(question, answer1, answer2, answer3, answer4, correct_Answer, id_Category)
VALUES ("q3", "a1","a2","a3","a4", 1, 2);
INSERT INTO Questions(question, answer1, answer2, answer3, answer4, correct_Answer, id_Category)
VALUES ("q4", "a1","a2","a3","a4", 1, 2);
INSERT INTO Questions(question, answer1, answer2, answer3, answer4, correct_Answer, id_Category)
VALUES ("q5", "a1","a2","a3","a4", 1, 2);

/* Programming */
INSERT INTO Questions(question, answer1, answer2, answer3, answer4, correct_Answer, id_Category)
VALUES ("q1", "a1","a2","a3","a4", 1, 3);
INSERT INTO Questions(question, answer1, answer2, answer3, answer4, correct_Answer, id_Category)
VALUES ("q2", "a1","a2","a3","a4", 1, 3);
INSERT INTO Questions(question, answer1, answer2, answer3, answer4, correct_Answer, id_Category)
VALUES ("q3", "a1","a2","a3","a4", 1, 3);
INSERT INTO Questions(question, answer1, answer2, answer3, answer4, correct_Answer, id_Category)
VALUES ("q4", "a1","a2","a3","a4", 1, 3);
INSERT INTO Questions(question, answer1, answer2, answer3, answer4, correct_Answer, id_Category)
VALUES ("q5", "a1","a2","a3","a4", 1, 3);

/* Movies */
INSERT INTO Questions(question, answer1, answer2, answer3, answer4, correct_Answer, id_Category)
VALUES ("q1", "a1","a2","a3","a4", 1, 4);
INSERT INTO Questions(question, answer1, answer2, answer3, answer4, correct_Answer, id_Category)
VALUES ("q2", "a1","a2","a3","a4", 1, 4);
INSERT INTO Questions(question, answer1, answer2, answer3, answer4, correct_Answer, id_Category)
VALUES ("q3", "a1","a2","a3","a4", 1, 4);
INSERT INTO Questions(question, answer1, answer2, answer3, answer4, correct_Answer, id_Category)
VALUES ("q4", "a1","a2","a3","a4", 1, 4);
INSERT INTO Questions(question, answer1, answer2, answer3, answer4, correct_Answer, id_Category)
VALUES ("q5", "a1","a2","a3","a4", 1, 4);
