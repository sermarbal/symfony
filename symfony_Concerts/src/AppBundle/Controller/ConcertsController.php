<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Concert;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class ConcertsController extends Controller {

    /**
     * @Route("/insertConcert", name="insertConcert")
     */
    public function insertConcertAction(Request $request) {

        $concert = new Concert();

        $form = $this->createFormBuilder($concert)
                ->add('nom', TextType::class)
                ->add('grup', TextType::class)
                ->add('data', DateType::class)
                ->add('ciutat', TextType::class)
                ->add('espai', TextType::class)
                ->add('save', SubmitType::class, array('label' => 'Insert'))
                ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($concert);
            $em->flush();
            return $this->render('default/message.html.twig', array(
                        'message' => 'Product inserted: ' . $concert->getId()
            ));
        }
        return $this->render('default/form.html.twig', array(
                    'title' => 'Insert Concert',
                    'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/selectAllConcerts", name="selectAllConcerts")
     */
    public function selectAllConcertsAction() {
        $concerts = $this->getDoctrine()
                ->getRepository('AppBundle:Concert')
                ->findAll();

        if (count($concerts) == 0) {
            return $this->render('default/message.html.twig', array(
                        'message' => 'No products found'));
        }
        return $this->render('concert/content.html.twig', array(
                    'concerts' => $concerts));
    }

    /**
     * @Route("/selectConcert", name="selectConcert")
     */
    public function selectConcertAction(Request $request) {
        $concert = new Concert();

        $form = $this->createFormBuilder($concert)
                ->add('nom', TextType::class)
                ->add('save', SubmitType::class, array('label' => 'Select'))
                ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $concerts = $this->getDoctrine()
                    ->getRepository('AppBundle:Concert')
                    ->findByNom($concert->getNom());
            if (count($concerts) == 0) {
                return $this->render('default/message.html.twig', array(
                            'message' => 'No concerts found'));
            }
            return $this->render('concert/content.html.twig', array(
                        'concerts' => $concerts));
        }
        return $this->render('default/form.html.twig', array(
                    'title' => 'Select Concert',
                    'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/updateConcert", name="updateConcert")
     */
    public function updateConcertAction(Request $request) {
        $concert = new Concert();

        $form = $this->createFormBuilder($concert)
                ->add('nom', TextType::class)
                ->add('save', SubmitType::class, array('label' => 'Select'))
                ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $concert = $em->getRepository('AppBundle:Concert')
                    ->findOneByNom($concert->getNom());
            if (!$concert) {
                return $this->render('default/message.html.twig', array(
                            'message' => 'No concerts found for nom ' . $concert->getNom()));
            }
            return $this->redirectToRoute('editConcert', array('nom' => $concert->getNom()));
        }
        return $this->render('default/form.html.twig', array(
                    'title' => 'Update Concert',
                    'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/editConcert/{nom}", name="editConcert")
     */
    public function editConcertAction($nom, Request $request) {
        $em = $this->getDoctrine()->getManager();
        $concert = $em->getRepository('AppBundle:Concert')
                ->findOneByNom($nom);

        $form = $this->createFormBuilder($concert)
                ->add('nom', TextType::class)
                ->add('grup', TextType::class)
                ->add('data', DateType::class)
                ->add('ciutat', TextType::class)
                ->add('espai', TextType::class)
                ->add('save', SubmitType::class, array('label' => 'Update'))
                ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();
            return $this->render('default/message.html.twig', array(
                        'message' => 'Concert updated id: ' . $concert->getId()
            ));
        }
        return $this->render('default/form.html.twig', array(
                    'title' => 'Update Concert',
                    'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/removeConcert", name="removeConcert")
     */
    public function removeConcertAction(Request $request) {
        $concert = new Concert();

        $form = $this->createFormBuilder($concert)
                ->add('nom', TextType::class)
                ->add('save', SubmitType::class, array('label' => 'Remove'))
                ->getForm();

        $form->handleRequest($request);

        $nom = $concert->getNom();

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $concert = $em->getRepository('AppBundle:Concert')
                    ->findOneByNom($concert->getNom());
            if (!$concert) {
                return $this->render('default/message.html.twig', array(
                            'message' => 'No concerts found for nom ' . $nom));
            }
            $id = $concert->getId();
            $em->remove($concert);
            $em->flush();
            return $this->render('default/message.html.twig', array(
                        'message' => 'Concert deleted: ' . $id
            ));
        }
        return $this->render('default/form.html.twig', array(
                    'title' => 'Remove Concert',
                    'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/selectConcertByDates", name="selectConcertByDates")
     */
    public function selectConcertByDatesAction(Request $request) {
        
        $defaultData = array('message' => 'Introduce the dates: ');

        $form = $this->createFormBuilder($defaultData)
                ->add('data1', DateType::class)
                ->add('data2', DateType::class)
                ->add('save', SubmitType::class, array('label' => 'Select'))
                ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $data = $form->getData();
            $concerts = $em->getRepository('AppBundle:Concert')
                    ->selectConcertByDates($data['data1'],$data['data2']);
            return $this->render('concert/content.html.twig', array(
                        'concerts' => $concerts));
        }
        return $this->render('default/form.html.twig', array(
                    'title' => 'Select Concert',
                    'form' => $form->createView(),
        ));
        
    }

}
