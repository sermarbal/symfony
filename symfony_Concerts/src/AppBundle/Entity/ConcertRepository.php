<?php

namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;

class ConcertRepository extends EntityRepository{
    
    public function selectConcertByDates($data1, $data2)
    {
        $d1 = $data1->format('Y-m-d');
        $d2 = $data2->format('Y-m-d');
        
        return $this->getEntityManager()
            ->createQuery(
                "SELECT p FROM AppBundle:Concert p WHERE p.data between '".$d1."' AND '".$d2."' ORDER BY p.nom ASC"
            )
            ->getResult();
    }
    
}
