<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="Concerts")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\ConcertRepository")
 */
class Concert {
    
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\Column(type="string", length=100, unique=true)
     */
    protected $nom;
    
    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $grup;
    
    /**
     * @ORM\Column(type="date")
     */
    protected $data;
    
    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $ciutat;
    
    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $espai;
    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Concert
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set grup
     *
     * @param string $grup
     *
     * @return Concert
     */
    public function setGrup($grup)
    {
        $this->grup = $grup;

        return $this;
    }

    /**
     * Get grup
     *
     * @return string
     */
    public function getGrup()
    {
        return $this->grup;
    }

    /**
     * Set data
     *
     * @param \DateTime $data
     *
     * @return Concert
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return \DateTime
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set ciutat
     *
     * @param string $ciutat
     *
     * @return Concert
     */
    public function setCiutat($ciutat)
    {
        $this->ciutat = $ciutat;

        return $this;
    }

    /**
     * Get ciutat
     *
     * @return string
     */
    public function getCiutat()
    {
        return $this->ciutat;
    }

    /**
     * Set espai
     *
     * @param string $espai
     *
     * @return Concert
     */
    public function setEspai($espai)
    {
        $this->espai = $espai;

        return $this;
    }

    /**
     * Get espai
     *
     * @return string
     */
    public function getEspai()
    {
        return $this->espai;
    }
}
