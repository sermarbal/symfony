<?php

namespace AppBundle\Controller\Examples\LuckyNumbers;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class LuckyController1 extends Controller
{
    /**
     * @Route("/lucky1", name="lucky1")
     */
    public function indexAction()
    {
        $n = rand(0,100);
        return new Response("<html><body>Lucky number: ".$n."</body></html>");
    }
}
