<?php

namespace AppBundle\Controller\Examples\LuckyNumbers;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class LuckyController5 extends Controller {

    /**
     * @Route("/lucky5/{count}", name="lucky5")
     */
    public function indexAction($count) {
        $numbers = array();

        for ($i = 0; $i < $count; $i++) {
            $numbers[] = rand(0, 100);
        }

        $numbersList = implode(', ', $numbers);

        $html = $this->container->get('templating')->render(
                'lucky/number.html.twig',
                array('luckyNumberList' => $numbersList)
        );
        
        return new Response ($html);
        
        /*
        return $this->render(
                'lucky/number.html.twig',
                array('luckyNumberList' => $numbersList)
        );
        */
    }
    
    

}
