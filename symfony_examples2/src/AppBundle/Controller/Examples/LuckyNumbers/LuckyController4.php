<?php

namespace AppBundle\Controller\Examples\LuckyNumbers;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class LuckyController4 extends Controller {

    /**
     * @Route("/lucky4/{count}", name="lucky4")
     */
    public function indexAction($count) {
        $numbers = array();

        for ($i = 0; $i < $count; $i++) {
            $numbers[] = rand(0, 100);
        }

        $numbersList = implode(', ', $numbers);

        return new Response(
                "<html><body>Lucky numbers: " . $numbersList . "</body></html>"
        );
    }

}
