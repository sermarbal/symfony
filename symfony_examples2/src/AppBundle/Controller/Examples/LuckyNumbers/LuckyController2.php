<?php

namespace AppBundle\Controller\Examples\LuckyNumbers;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class LuckyController2 extends Controller
{
    /**
     * @Route("/lucky2", name="lucky2")
     */
    public function indexAction()
    {
        $data = array(
            'lucky_number' => rand(0,100),
        );
        
        return new Response(
            json_encode($data),
            200,
            array('Content-Type' => 'application/json')
        );
        
    }
}
