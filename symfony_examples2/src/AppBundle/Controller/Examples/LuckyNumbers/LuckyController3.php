<?php

namespace AppBundle\Controller\Examples\LuckyNumbers;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;


class LuckyController3 extends Controller
{
    /**
     * @Route("/lucky3", name="lucky3")
     */
    public function indexAction()
    {
        $data = array(
            'lucky_number' => rand(0,100),
        );
        
        return new JsonResponse($data);
        
    }
}
