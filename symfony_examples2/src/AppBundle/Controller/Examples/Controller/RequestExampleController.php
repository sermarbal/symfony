<?php

namespace AppBundle\Controller\Examples\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class RequestExampleController {

    /**
     * @Route("/request", name="request")
     */
    public function indexAction(Request $request) {
        return new Response('The request: <br>'. $request);
    }
    
}
