<?php

namespace AppBundle\Controller\Examples\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ExceptionsExampleController extends Controller {

    /**
     * @Route("/exception", name="exception")
     */
    public function indexAction() {
    // retrieve the object from database
    $product = null;
    //$product = "notnull";
    
    if(!$product){
        throw $this->createNotFoundException('The product does not exist');
    }
    
    return $this-> render();
    }
    
}
