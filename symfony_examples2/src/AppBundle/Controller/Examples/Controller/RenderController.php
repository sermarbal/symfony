<?php

namespace AppBundle\Controller\Examples\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class RenderController extends Controller{
    
    /**
     * @Route("/render", name="render")
     */
    public function indexAction($name) {
        // renders app/Resources/views/hello/index.html.twig
        return $this->render('hello/index.html.twig', array('name' => $name));
    }
    
}