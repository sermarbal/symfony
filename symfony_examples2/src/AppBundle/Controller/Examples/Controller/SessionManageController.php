<?php

namespace AppBundle\Controller\Examples\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SessionManageController extends Controller {

    /**
     * @Route("/session", name="session")
     */
    public function indexAction(Request $request) {
        
        $session = $request-> getSession();
        
        // store an atribute for reuse during a later user request.
        $session->set('foo', 'bar');
        
        // get the attriubte set by another controller in another request
        $foobar = $session->get('foobar');
        $filters = $session->get('filters', array());
        
        array_push($filters, "apple", "raspberry");
        return new Response('foobar: '.$foobar.'<br>filters: '.print_r($filters));
    }
    
}
