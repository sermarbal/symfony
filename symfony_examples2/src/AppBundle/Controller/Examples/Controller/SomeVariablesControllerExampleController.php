<?php

namespace AppBundle\Controller\Examples\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

class SomeVariablesControllerExampleController {

    /**
     * @Route("/hello/{firstName}/{lastName}", name="fullhello")
     */
    public function indexAction($firstName, $lastName, $foo = 'foo') {
        return new Response('<html><body>Hello ' . $firstName .' '.$lastName.' '.$foo.'!</body></html>');
    }
}
