<?php

namespace AppBundle\Controller\Examples\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

class SimpleControllerExampleController {

    /**
     * @Route("/hello/{name}", name="hello")
     */
    public function indexAction($name) {
        return new Response('<html><body>Hello '.$name.'!</body></html>');
    }
    
}
