<?php

namespace AppBundle\Controller\Examples\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class RedirectingExampleController extends Controller {

    /**
     * @Route("/redirect", name="redirect")
     */
    public function indexAction() {
        return $this->redirectToRoute('hello', ['name' => 'Sergi']);
    }
    
    // External link
    /**
     * @Route("/redirect", name="redirect")
     *//*
    public function indexAction() {
        return $this->redirectToRoute('http://symfony.com/doc');
    }*/
    
}
