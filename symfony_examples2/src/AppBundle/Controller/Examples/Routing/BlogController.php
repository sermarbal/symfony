<?php

namespace AppBundle\Controller\Examples\Routing;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

class BlogController extends Controller {

    // \d+ es una expresió regular que defineix que es requereix que la pagina sigui un digit.
    /**
     * @Route("/blog/{page}", name="blog_show", defaults={"page"=1}, requirements={"page": "\d+"})
     */
    public function showAction($page) {
        return new Response("<html><body>Blog pagina: ".$page."</body></html>");
    }

}

