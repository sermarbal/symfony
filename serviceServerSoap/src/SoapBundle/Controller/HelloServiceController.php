<?php

namespace SoapBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class HelloServiceController extends Controller
{
    
    /**
     * @Route("/soap")
     */
    public function indexAction()
    {
        // This link in the navigator!!! http://192.168.1.133:8000/soap?wsdl
        $server = new \SoapServer('hello.wsdl');
        $server->setObject($this->get('hello_service'));

        $response = new Response();
        $response->headers->set('Content-Type', 'text/xml; charset=ISO-8859-1');

        ob_start();
        $server->handle();
        $response->setContent(ob_get_clean());

        return $response;
    }
}