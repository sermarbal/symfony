<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Product;
use AppBundle\Entity\Category;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class ProductController extends Controller
{

    /**
     * @Route("/insertProduct", name="insertProduct")
     */
    public function insertProductAction(Request $request)
    {
        $product = new Product();

        $form = $this->createFormBuilder($product)
            ->add('name', TextType::class)
            ->add('price', NumberType::class)
            ->add('description', TextareaType::class)
            ->add('category',EntityType::class, array('class' => 'AppBundle:Category','choice_label' => 'name'))
            ->add('features',EntityType::class, array('class' => 'AppBundle:Feature',
                  'choice_label' => 'name', 'multiple'=> true, 'expanded'=>true))
            ->add('save', SubmitType::class, array('label' => 'Create'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($product);
            $em->flush();
            return $this->render('default/message.html.twig', array(
                'message' => 'Product inserted: '. $product->getId()
            ));
        }
        return $this->render('default/form.html.twig', array(
            'title' => 'Insert Product',
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/selectProduct", name="selectProduct")
     */
    public function selectProductAction(Request $request)
    {
        $product = new Product();

        $form = $this->createFormBuilder($product)
            ->add('name', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Select'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $products = $this->getDoctrine()
                ->getRepository('AppBundle:Product')
                ->findByName($product->getName());
            if (count($products)==0) {
                return $this->render('default/message.html.twig', array(
                    'message' => 'No products found'));
            }
            return $this->render('product/content.html.twig', array(
                'products' => $products));
        }
        return $this->render('default/form.html.twig', array(
            'title' => 'Select Product',
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/selectAllProducts", name="selectAllProducts")
     */
    public function selectAllProductsAction()
    {
        $products = $this->getDoctrine()
            ->getRepository('AppBundle:Product')
            ->findAll();

        if (count($products)==0) {
            return $this->render('default/message.html.twig', array(
                'message' => 'No products found'));
        }
        return $this->render('product/content.html.twig', array(
            'products' => $products));
    }

    /**
     * @Route("/selectProductsByCategory", name="selectProductsByCategory")
     */
    public function selectProductsByCategoryAction(Request $request)
    {

        $product = new Product();

        $form = $this->createFormBuilder($product)
            ->add('category',EntityType::class, array('class' => 'AppBundle:Category','choice_label' => 'name'))
            ->add('save', SubmitType::class, array('label' => 'Select'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /* $category = $this->getDoctrine()
                ->getRepository('AppBundle:Category')
                ->findOneByName($product->getCategory()->getName()); */

            $products = $product->getCategory()->getProducts();
            if (count($products)==0) {
                throw $this->createNotFoundException(
                    'No products found '
                );
            }
            return $this->render('product/content.html.twig', array(
                'products' => $products));
        }
        return $this->render('default/form.html.twig', array(
            'title' => 'Select Product By Category',
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/updateProduct", name="updateProduct")
     */
    public function updateProductAction(Request $request)
    {
        $product = new Product();

        $form = $this->createFormBuilder($product)
            ->add('name', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Select'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $product = $em->getRepository('AppBundle:Product')
                ->findOneByName($product->getName());
            if (!$product) {
                return $this->render('default/message.html.twig', array(
                    'message' => 'No products found for name '. $product->getName()));
            }
            return $this->redirectToRoute('editProduct',array('name' => $product->getName()));

        }
        return $this->render('default/form.html.twig', array(
            'title' => 'Update Product',
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/editProduct/{name}", name="editProduct")
     */
    public function editProductAction($name, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository('AppBundle:Product')
           ->findOneByName($name);

        $form = $this->createFormBuilder($product)
            ->add('name', TextType::class)
            ->add('price', NumberType::class)
            ->add('description', TextareaType::class)
            ->add('Category', EntityType::class, array('class' => 'AppBundle:Category', 'choice_label' => 'name'))
            ->add('features',EntityType::class, array('class' => 'AppBundle:Feature',
                'choice_label' => 'name', 'multiple'=> true, 'expanded'=>true))
            ->add('save', SubmitType::class, array('label' => 'Update'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();
            return $this->render('default/message.html.twig', array(
                'message' => 'Product updated id: '. $product->getId()
            ));
        }
        return $this->render('default/form.html.twig', array(
            'title' => 'Update Product',
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/removeProduct", name="removeProduct")
     */
    public function removeProductAction(Request $request)
    {
        $product = new Product();

        $form = $this->createFormBuilder($product)
            ->add('name', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Remove'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $product = $em->getRepository('AppBundle:Product')
                      ->findOneByName($product->getName());
            if (!$product) {
                return $this->render('default/message.html.twig', array(
                    'message' => 'No products found for name '. $product->getName()));
            }
            $id = $product->getId();
            $em->remove($product);
            $em->flush();
            return $this->render('default/message.html.twig', array(
                'message' => 'Product deleted: '. $id
            ));

        }
        return $this->render('default/form.html.twig', array(
            'title' => 'Remove Product',
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/selectAllProductsOrderedByName", name="selectAllProductsOrderedByName")
     */
    public function selectAllProductsOrderedByNameAction()
    {
        $products = $this->getDoctrine()
            ->getRepository('AppBundle:Product')
            ->findAllOrderedByName();

        
        
        if (count($products)==0) {
            return $this->render('default/message.html.twig', array(
                'message' => 'No products found'));
        }
        return $this->render('product/content.html.twig', array(
            'products' => $products));
    }
}
