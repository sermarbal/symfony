<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Customer;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class CustomerController extends Controller
{

    /**
     * @Route("/insertCustomer", name="insertCustomer")
     */
    public function insertCustomerAction(Request $request)
    {
        $customer = new Customer();

        $form = $this->createFormBuilder($customer)
            ->add('name', TextType::class)
            ->add('address', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Create'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($customer);
            $em->flush();
            return $this->render('default/message.html.twig', array(
                'message' => 'Customer inserted: '. $customer->getId()
            ));
        }
        return $this->render('default/form.html.twig', array(
            'title' => 'Insert Customer',
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/selectCustomer", name="selectCustomer")
     */
    public function selectCustomerAction(Request $request)
    {
        $customer = new Customer();

        $form = $this->createFormBuilder($customer)
            ->add('name', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Select'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $customers = $this->getDoctrine()
                ->getRepository('AppBundle:Customer')
                ->findByName($customer->getName());
            if (count($customers)==0) {
                return $this->render('default/message.html.twig', array(
                    'message' => 'No customers found'));
            }
            return $this->render('customer/content.html.twig', array(
                'customers' => $customers));
        }
        return $this->render('default/form.html.twig', array(
            'title' => 'Select Customer',
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/selectAllCustomers", name="selectAllCustomers")
     */
    public function selectAllCustomersAction()
    {
        $customers = $this->getDoctrine()
            ->getRepository('AppBundle:Customer')
            ->findAll();

        if (count($customers)==0) {
            return $this->render('default/message.html.twig', array(
                'message' => 'No customers found'));
        }
        return $this->render('customer/content.html.twig', array(
            'customers' => $customers));
    }

    /**
     * @Route("/updateCustomer", name="updateCustomer")
     */
    public function updateCustomerAction(Request $request)
    {
        $customer = new Customer();

        $form = $this->createFormBuilder($customer)
            ->add('name', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Select'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $customer = $em->getRepository('AppBundle:Customer')
                ->findOneByName($customer->getName());
            if (!$customer) {
                return $this->render('default/message.html.twig', array(
                    'message' => 'No customers found'));
            }
            return $this->redirectToRoute('editCustomer',array('name' => $customer->getName()));

        }
        return $this->render('default/form.html.twig', array(
            'title' => 'Update Customer',
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/editCustomer/{name}", name="editCustomer")
     */
    public function editCustomerAction($name, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $customer = $em->getRepository('AppBundle:Customer')
           ->findOneByName($name);

        $form = $this->createFormBuilder($customer)
            ->add('name', TextType::class)
            ->add('address', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Update'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();
            return $this->render('default/message.html.twig', array(
                'message' => 'Customer updated id: '. $customer->getId()
            ));
        }
        return $this->render('default/form.html.twig', array(
            'title' => 'Update Customer',
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/removeCustomer", name="removeCustomer")
     */
    public function removeCustomerAction(Request $request)
    {
        $customer = new Customer();

        $form = $this->createFormBuilder($customer)
            ->add('name', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Remove'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $customer = $em->getRepository('AppBundle:Customer')
                ->findOneByName($customer->getName());
            if (!$customer) {
                return $this->render('default/message.html.twig', array(
                    'message' => 'No products found for name '. $customer->getName()));
            }
            $id = $customer->getId();
            $em->remove($customer);
            $em->flush();
            return $this->render('default/message.html.twig', array(
                'message' => 'Customer deleted: '. $id
            ));

        }
        return $this->render('default/form.html.twig', array(
            'title' => 'Remove Customer',
            'form' => $form->createView(),
        ));
    }
}
